package com.nttdata.spring.repository;

import java.util.Arrays;
import java.util.Iterator;

public class Edificio {
	
	private String[] plantas;

	/**
	 * Constructor de la clase Edificio
	 * Se inicializa con el número de plantas del edificio
	 * Todas las plantas se establecen como vacías
	 * @param numPlantas Número de plantas del edificio
	 */
	public Edificio(int numPlantas) {
		super();
		this.plantas = new String[numPlantas];
		
		for (int i = 0; i < plantas.length; i++) {
			plantas[i] = "Vacía";
		}
	}
	
	public void agregarPersona(int planta, String nomApe) {
		this.plantas[planta] = nomApe;
	}
	
	public void buscarPersona(String nomApe) {
		String result = nomApe + " no ha sido encontrado en el edificio";
		for (int i = 0; i < this.plantas.length; i++) {
			if(this.plantas[i] != null && this.plantas[i].equals(nomApe))
				result = nomApe + " ha sido encontrada en la planta " + i;
		}
		System.out.println(result);
	}
	
	public String[] getPlantas() {
		return plantas;
	}

	@Override
	public String toString() {
		return "Edificio [plantas=" + Arrays.toString(plantas) + "]";
	}
	
	
}
