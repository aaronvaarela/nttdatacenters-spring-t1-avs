package com.nttdata.spring.services;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.nttdata.spring.repository.Edificio;

@Primary
@Qualifier("viviendas")
@Service
public class EdificioServiceImpl implements EdificioServiceI {

	@Override
	public Edificio crearEdificio(int numPlantas) throws Exception {
		Edificio edificio = new Edificio(numPlantas);
		return edificio;
	}

	@Override
	public void agregarPersonaEdificioEdificioPlanta(Edificio edificio, int numPlantas, String nombreApellidos)
			throws Exception {
		edificio.agregarPersona(numPlantas, nombreApellidos);
		
	}

	@Override
	public void buscarPersona(Edificio edificio, String nombreApellidos) throws Exception {
		edificio.buscarPersona(nombreApellidos);
	}

}
