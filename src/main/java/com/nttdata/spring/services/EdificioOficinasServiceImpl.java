package com.nttdata.spring.services;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.nttdata.spring.repository.Edificio;

@Qualifier("oficinas")
@Service
public class EdificioOficinasServiceImpl implements EdificioServiceI {

	@Override
	public Edificio crearEdificio(int numPlantas) throws Exception {
		Edificio oficinas = new Edificio(numPlantas + 1);
		oficinas.agregarPersona(0, "PARKING");
		return oficinas;
	}

	@Override
	public void agregarPersonaEdificioEdificioPlanta(Edificio edificio, int numPlantas, String nombreApellidos)
			throws Exception {
			edificio.agregarPersona(numPlantas + 1, nombreApellidos);
	}

	@Override
	public void buscarPersona(Edificio edificio, String nombreApellidos) throws Exception {
		edificio.buscarPersona(nombreApellidos);
	}

}
