package com.nttdata.spring.services;

import com.nttdata.spring.repository.Edificio;

/**
 * Servicio para la gestión de los edificios
 * @author Aaron
 *
 */
public interface EdificioServiceI {
	
	/**
	 * Crea un nuevo edificio con las plantas que se le indica
	 * @param numPlantas Entero con el número de plantas del edificio
	 * @return {@link Edificio} creado
	 */
	public Edificio crearEdificio(int numPlantas) throws Exception;
	
	/**
	 * Pasado un edificio se agrega una persona en la planta indicada
	 * @param edificio {@link Edificio} edificio
	 * @param numPlantas Entero con el número de planta donde agregar el edificio
	 * @param nombreApellidos {@link String} con el nombre y apellidos de la persona
	 */
	
	/**
	 * Pasado un edificio se agrega una persona en la planta indicada
	 * @param edificio {@link Edificio} edificio
	 * @param numPlantas Entero con el número de planta donde agregar el edificio
	 * @param nombreApellidos {@link String} con el nombre y apellidos de la persona
	 * @throws Exception En caso de error
	 */
	public void agregarPersonaEdificioEdificioPlanta(Edificio edificio, int numPlantas, String nombreApellidos) throws Exception;
	
	/**
	 * Busca si la persona indicada habita en el edificio
	 * @param edificio {@link Edificio} edificio
	 * @param nombreApellidos {@link String} con el nombre y apellidos de la persona a buscar
	 * @throws Exception En caso de error
	 */
	public void buscarPersona(Edificio edificio, String nombreApellidos) throws Exception;
}
