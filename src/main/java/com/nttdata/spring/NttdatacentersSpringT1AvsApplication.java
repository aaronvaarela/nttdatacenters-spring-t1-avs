package com.nttdata.spring;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.nttdata.spring.repository.Edificio;
import com.nttdata.spring.services.EdificioServiceI;

@SpringBootApplication
public class NttdatacentersSpringT1AvsApplication implements CommandLineRunner {

	@Autowired
	private EdificioServiceI edificioService;

	@Autowired
	@Qualifier("oficinas")
	private EdificioServiceI edificioServiceOficinas;

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(NttdatacentersSpringT1AvsApplication.class, args);
		// ctx.close();
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Inicio Aplicación");

		List<Edificio> listaEdificios = new ArrayList<>();

		Edificio edificioTaller1 = edificioService.crearEdificio(6);
		Edificio edificioCallePrincipal = edificioServiceOficinas.crearEdificio(6);

		listaEdificios.add(edificioTaller1);
		listaEdificios.add(edificioCallePrincipal);

		// Agregar personas al edificio Taller
		edificioService.agregarPersonaEdificioEdificioPlanta(edificioTaller1, 0, "Aaron Varela");
		edificioService.agregarPersonaEdificioEdificioPlanta(edificioTaller1, 1, "Alberto Lorenzo");
		edificioService.agregarPersonaEdificioEdificioPlanta(edificioTaller1, 2, "Angel Gonzalez");
		edificioService.agregarPersonaEdificioEdificioPlanta(edificioTaller1, 3, "Antonio Navas");

		// Agregar persona al edificio Principal
		edificioServiceOficinas.agregarPersonaEdificioEdificioPlanta(edificioCallePrincipal, 0, "Angel Gonzalez");
		edificioServiceOficinas.agregarPersonaEdificioEdificioPlanta(edificioCallePrincipal, 1, "Antonio Navas");
		/*
		 * // Buscar personas en edificio Taller
		 * System.out.println("\nBúsqueda en oficinas");
		 * edificioServiceOficinas.buscarPersona(edificioTaller1, "Aaron Varela");
		 * edificioServiceOficinas.buscarPersona(edificioTaller1, "Angel Gonzalez");
		 * 
		 * // Buscar personas en edificio Principal
		 * System.out.println("\nBúsqueda en viviendas");
		 * edificioService.buscarPersona(edificioCallePrincipal, "Angel Gonzalez");
		 * edificioService.buscarPersona(edificioCallePrincipal, "Antonio Navas");
		 */
		
		Scanner input = new Scanner(System.in);
		String op;

		// Se le muestra al usuario una serie de opciones que puede elegir
		// Al realizar una acción se vuelve a mostrar el menú hasta que decida cerrar la
		// aplicación (eligiendo la última opción)
		do {
			System.out.println("--------------------------------------------------------------");
			System.out.println("Seleccione una opción");
			System.out.println("1. Listar todos los edificios");
			System.out.println("2. Buscar persona en todos los edificios");
			System.out.println("3. Crear un nuevo edificio");
			System.out.println("4. Agregar persona al edificio");
			System.out.println("0. Salir");

			op = input.nextLine();
			String nombrePersona;
			String[] plantas;
			int i = 0;
			int j = 0;

			switch (op) {
			case "1":
				System.out.println("--1. Listar todos los edificios--");
				i = 1;
				for (Edificio edificio : listaEdificios) {
					plantas = edificio.getPlantas();
					System.out.print("\nEdificio " + i);
					if (plantas[0].equalsIgnoreCase("PARKING"))
						System.out.print(" (oficinas)\n");
					else
						System.out.print(" (viviendas)\n");

					for (j = 0; j < plantas.length; j++) {
						System.out.println("\tPlanta " + (j + 1) + ": " + plantas[j]);
					}
					i++;
				}

				System.out.println("");
				break;
			case "2":
				System.out.println("--2. Buscar persona en todos los edificios--");

				System.out.println("Introduzca el nombre de la persona");
				nombrePersona = input.nextLine().toLowerCase();

				if (nombrePersona != null && nombrePersona.length() > 0) {
					i = 1;

					for (Edificio edificio : listaEdificios) {
						plantas = edificio.getPlantas();
						for (j = 0; j < plantas.length; j++) {
							if (plantas[j].toLowerCase().contains(nombrePersona)) {
								System.out.println(nombrePersona + " encontrado/a en:");
								
								System.out.print("Edificio " + i);
								if (plantas[0].equalsIgnoreCase("PARKING"))
									System.out.print(" (oficinas)\n");
								else
									System.out.print(" (viviendas)\n");

								for (int g = 0; g < plantas.length; g++) {
									System.out.println("\tPlanta " + (g + 1) + ": " + plantas[g]);
								}
								break;
							}
							j++;
						}
						i++;
					}
				} else {
					System.out.println("Nombre no válido");
				}

				System.out.println();
				break;
			case "3":
				System.out.println("--3. Crear un nuevo edificio--");
				System.out.println("Edificio de oficinas o viviendas");
				String tipo = input.nextLine();

				System.out.println("Introduzca el número de plantas");
				String numPlantas = input.nextLine();

				if (tipo.toLowerCase().contains("ofi"))
					listaEdificios.add(edificioServiceOficinas.crearEdificio(Integer.parseInt(numPlantas)));
				else if (tipo.toLowerCase().contains("viv"))
					listaEdificios.add(edificioService.crearEdificio(Integer.parseInt(numPlantas)));
				else
					System.out.println("No has introducido un tipo correcto");

				break;
			case "4":
				System.out.println("--4. Agregar persona al edificio--");
				i = 1;
				// Listar todos los edificios y sus plantas
				for (Edificio edificio : listaEdificios) {
					plantas = edificio.getPlantas();
					System.out.print("\nEdificio " + i);
					if (plantas[0].equalsIgnoreCase("PARKING"))
						System.out.print(" (oficinas)\n");
					else
						System.out.print(" (viviendas)\n");

					for (j = 0; j < plantas.length; j++) {
						System.out.println("\tPlanta " + (j + 1) + ": " + plantas[j]);
					}
					i++;
				}
				
				// Usuario selecciona el edificio y validación
				System.out.println("Introduzca el número de edificio");
				String numEdificio = input.nextLine();
				Edificio edificioSeleccionado = listaEdificios.get(Integer.parseInt(numEdificio) - 1);

				if (edificioSeleccionado != null) {

					// Usuario selecciona la planta y validación
					System.out.println("Introduzca la planta");
					int numPlanta = Integer.parseInt(input.nextLine()) - 1;
					plantas = edificioSeleccionado.getPlantas();

					if (numPlanta < plantas.length) {

						// Comprobar si la planta está vacía o no
						if (!plantas[numPlanta].equalsIgnoreCase("Vacía")) {
							System.out.println("La planta no está vacía");
						} else {
							System.out.println("Introduzca el nombre de la persona");
							nombrePersona = input.nextLine();
							edificioService.agregarPersonaEdificioEdificioPlanta(edificioSeleccionado, numPlanta, nombrePersona);
						}
					} else {
						System.out.println("Esa planta no existe");
					}
				} else {
					System.out.println("Ese edificio no existe");
				}

				break;
			case "0":
				System.out.println("Adiós");
				break;
			default:
				System.out.println("Opción no válida");
				break;
			}

		} while (!op.equalsIgnoreCase("0"));

		input.close();
	}

}
